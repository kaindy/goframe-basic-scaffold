package aes

import (
	"bytes"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"github.com/gogf/gf/v2/frame/g"
)

// 基于DES的对称加解密，用于将资源的ID进行加密和解密

// 从配置中读取 secretKey
var secretKey = g.Cfg().MustGet(context.TODO(), "aes.SecretKey").String()
var byteKey = []byte(secretKey)

func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padText...)
}

func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unPadding := int(origData[length-1])
	return origData[:(length - unPadding)]
}

// SymmetricEncryptByAES AES加密,CBC
func SymmetricEncryptByAES(origData []byte) (string, error) {
	block, err := aes.NewCipher(byteKey)
	if err != nil {
		return "", err
	}
	blockSize := block.BlockSize()
	origData = PKCS7Padding(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, byteKey[:blockSize])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return base64.StdEncoding.EncodeToString(crypted), nil
}

// SymmetricDecryptByAES AES解密
func SymmetricDecryptByAES(crypted string) ([]byte, error) {
	block, err := aes.NewCipher(byteKey)
	if err != nil {
		return nil, err
	}
	byteCrypted, err := base64.StdEncoding.DecodeString(crypted)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, byteKey[:blockSize])
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, byteCrypted)
	origData = PKCS7UnPadding(origData)
	return origData, nil
}

/******************** 使用示例 ***********************
func main() {
	text := "123" // 你要加密的数据
	AesKey := []byte("#HvL%$o0oNNoOZnk#o2qbqCeQB1iXeIR") // 对称秘钥长度必须是16的倍数

	fmt.Printf("明文: %s\n秘钥: %s\n", text, string(AesKey))
	encrypted, err := AesEncrypt([]byte(text), AesKey)
	if err != nil {
		panic(err)
	}
	fmt.Printf("加密后: %s\n", base64.StdEncoding.EncodeToString(encrypted))
	// encrypteds, _ := base64.StdEncoding.DecodeString("j4H4Tv5VcXv0oNLwB/fr+g==")
	origin, err := AesDecrypt(encrypted, AesKey)
	if err != nil {
		panic(err)
	}
	fmt.Printf("解密后明文: %s\n", string(origin))
}

result:

	明文: 123
	秘钥: #HvL%$o0oNNoOZnk#o2qbqCeQB1iXeIR
	加密后: xvhqp8bT0mkEcAsNK+L4fw==
	解密后明文: 123

*/
