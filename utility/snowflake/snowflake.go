package snowflake

import (
	"fmt"
	"github.com/gogf/gf/v2/util/gconv"
	"time"

	sf "github.com/bwmarrin/snowflake"
)

var node *sf.Node

func InitSFInstance(startTime string, machineID int64) (err error) {
	//fmt.Printf("start_time: %#v\n", startTime)
	//fmt.Println("machineID", machineID)
	var st time.Time
	st, err = time.Parse("2006-01-02", startTime)
	if err != nil {
		fmt.Printf("init snowflake failed, error: %v\n", err)
		return
	}
	sf.Epoch = st.UnixNano() / 1000000
	node, err = sf.NewNode(machineID)
	return
}

func GenerateSFID() string {
	return gconv.String(node.Generate().Int64())
}
