package bizcode

import "github.com/gogf/gf/v2/errors/gcode"

var (
	CodeNil                       = gcode.New(0, "", nil)            // No error code specified.
	CodeOK                        = gcode.New(200, "OK", nil)        // It is OK.
	CodeInternalError             = gcode.New(500, "服务器内部错误", nil)   // An error occurred internally.
	CodeValidationFailed          = gcode.New(400, "数据验证失败", nil)    // Data validation failed.
	CodeDbOperationError          = gcode.New(506, "数据库操作失败", nil)   // Database operation error.
	CodeInvalidParameter          = gcode.New(418, "无效的参数", nil)     // The given parameter for current operation is invalid.
	CodeMissingParameter          = gcode.New(419, "缺失必要参数", nil)    // Parameter for current operation is missing.
	CodeInvalidOperation          = gcode.New(420, "无效的操作", nil)     // The function cannot be used like this.
	CodeInvalidConfiguration      = gcode.New(507, "无效的配置", nil)     // The configuration is invalid for current operation.
	CodeMissingConfiguration      = gcode.New(508, "缺少配置参数", nil)    // The configuration is missing for current operation.
	CodeNotImplemented            = gcode.New(510, "没有实现", nil)      // The operation is not implemented yet.
	CodeNotSupported              = gcode.New(511, "未支持的操作", nil)    // The operation is not supported yet.
	CodeOperationFailed           = gcode.New(512, "操作失败", nil)      // I tried, but I cannot give you what you want.
	CodeNotAuthorized             = gcode.New(401, "身份验证失败", nil)    // Not Authorized.
	CodeSecurityReason            = gcode.New(513, "安全策略", nil)      // Security Reason.
	CodeServerBusy                = gcode.New(514, "服务器繁忙", nil)     // Server is busy, please try again later.
	CodeUnknown                   = gcode.New(515, "未知错误", nil)      // Unknown error.
	CodeNotFound                  = gcode.New(404, "资源未找到", nil)     // Resource does not exist.
	CodeInvalidRequest            = gcode.New(403, "无效请求,拒绝执行", nil) // Invalid request.
	CodeNecessaryPackageNotImport = gcode.New(516, "未导入必要的依赖包", nil) // It needs necessary package import.
	CodeInternalPanic             = gcode.New(517, "内部Panic", nil)   // An panic occurred internally.
	CodeBusinessValidationFailed  = gcode.New(10000, "业务验证失败", nil)  // Business validation failed.
)
