package consts

const (
	FileMaxUploadCountMinute = 10 // 每分钟最大的上传次数
	// gToken相关
	TokenType      = "Bearer"
	GTokenExpireIn = 10 * 24 * 60 * 60
)
