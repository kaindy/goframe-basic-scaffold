package common

import (
	"context"
	"fmt"
	"goframe-basic-scaffold/utility/bizcode"
	"slices"
	"strings"

	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

// ==================================
// 公共的控制器代码逻辑
// ==================================

// VerifyUploadFileSize 验证文件大小
func VerifyUploadFileSize(ctx context.Context, file *ghttp.UploadFile) error {
	// 获取系统配置的上传文件大小限制
	maxSize := g.Cfg().MustGet(ctx, "upload.maxSize").Int64()
	if file.FileHeader.Size/1024/1024 > maxSize {
		return gerror.NewCode(bizcode.CodeInvalidParameter, fmt.Sprintf("文件大小超限,最大%dMB", maxSize))
	}
	return nil
}

// VerifyUploadFileType 验证文件类型
func VerifyUploadFileType(ctx context.Context, file *ghttp.UploadFile) error {
	// 获取文件上传类型限制并转换为slice切片类型
	typeLimitStr := g.Cfg().MustGet(ctx, "upload.fileTypeRestrictions").String()
	typeLimitSlice := strings.Split(typeLimitStr, ",")
	// 获取当前上传文件的类型
	fileType := file.FileHeader.Header.Get("content-type")
	// 判断当前上传文件类型是否包含在限制类型中
	// slices 包是 1.19.0 版本中包含的工具包，注意 golang 的版本
	if !slices.Contains(typeLimitSlice, fileType) {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "无效的上传文件类型")
	}
	return nil
}
