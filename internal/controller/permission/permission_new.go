// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package permission

import (
	"goframe-basic-scaffold/api/permission"
)

type ControllerV1 struct{}

func NewV1() permission.IPermissionV1 {
	return &ControllerV1{}
}
