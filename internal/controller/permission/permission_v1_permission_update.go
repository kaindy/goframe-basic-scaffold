package permission

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/permission/v1"
)

func (c *ControllerV1) PermissionUpdate(ctx context.Context, req *v1.PermissionUpdateReq) (res *v1.PermissionUpdateRes, err error) {
	err = service.Permission().Update(ctx, &model.PermissionUpdateInput{
		Id:   req.Id,
		Name: req.Name,
		Path: req.Path,
	})
	if err != nil {
		return nil, err
	}
	return &v1.PermissionUpdateRes{Id: req.Id}, nil
}
