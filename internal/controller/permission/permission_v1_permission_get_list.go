package permission

import (
	"context"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/permission/v1"
)

func (c *ControllerV1) PermissionGetList(ctx context.Context, req *v1.PermissionGetListReq) (res *v1.PermissionGetListRes, err error) {
	list, err := service.Permission().GetList(ctx, &model.PermissionGetListInput{
		CommonPaginationInput: model.CommonPaginationInput{
			PageNum:  req.PageNum,
			PageSize: req.PageSize,
		},
	})
	if err != nil {
		return nil, err
	}
	return &v1.PermissionGetListRes{
		Lists: list.Lists,
		PaginationRes: common.PaginationRes{
			PageNum:  list.PageNum,
			PageSize: list.PageSize,
			Total:    list.Total,
		},
	}, nil
}
