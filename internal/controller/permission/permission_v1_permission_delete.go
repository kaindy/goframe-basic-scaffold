package permission

import (
	"context"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/permission/v1"
)

func (c *ControllerV1) PermissionDelete(ctx context.Context, req *v1.PermissionDeleteReq) (res *v1.PermissionDeleteRes, err error) {
	err = service.Permission().Delete(ctx, req.Id)
	if err != nil {
		return nil, err
	}
	return &v1.PermissionDeleteRes{}, nil
}
