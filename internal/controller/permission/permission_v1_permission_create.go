package permission

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/permission/v1"
)

func (c *ControllerV1) PermissionCreate(ctx context.Context, req *v1.PermissionCreateReq) (res *v1.PermissionCreateRes, err error) {
	out, err := service.Permission().Create(ctx, &model.PermissionCreateInput{
		Name: req.Name,
		Path: req.Path,
	})
	if err != nil {
		return nil, err
	}
	return &v1.PermissionCreateRes{Id: out.Id}, nil
}
