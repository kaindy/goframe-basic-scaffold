package user

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/user/v1"
)

func (c *ControllerV1) UserCreate(ctx context.Context, req *v1.UserCreateReq) (res *v1.UserCreateRes, err error) {
	out, err := service.User().CreateUser(ctx, &model.UserCreateInput{
		Name:     req.Name,
		Password: req.Password,
		RoleIds:  req.RoleIds,
		IsAdmin:  req.IsAdmin,
	})
	if err != nil {
		return nil, err
	}
	return &v1.UserCreateRes{Uid: out.Uid}, nil
}
