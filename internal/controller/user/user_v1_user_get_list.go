package user

import (
	"context"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/user/v1"
)

func (c *ControllerV1) UserGetList(ctx context.Context, req *v1.UserGetListReq) (res *v1.UserGetListRes, err error) {
	list, err := service.User().GetUserList(ctx, &model.UserGetListInput{
		CommonPaginationInput: model.CommonPaginationInput{
			PageNum:  req.PageNum,
			PageSize: req.PageSize,
		},
	})
	if err != nil {
		return nil, err
	}
	return &v1.UserGetListRes{
		Lists: list.Lists,
		PaginationRes: common.PaginationRes{
			PageNum:  list.PageNum,
			PageSize: list.PageSize,
			Total:    list.Total,
		},
	}, nil
}
