package user

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/user/v1"
)

func (c *ControllerV1) UserUpdatePassword(ctx context.Context, req *v1.UserUpdatePasswordReq) (res *v1.UserUpdatePasswordRes, err error) {
	err = service.User().UpdateUserPassword(ctx, &model.UserUpdatePasswordInput{
		Uid:         req.Uid,
		OldPassword: req.OldPassword,
		Password:    req.Password,
	})
	if err != nil {
		return nil, err
	}
	return &v1.UserUpdatePasswordRes{}, nil
}
