package user

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/user/v1"
)

func (c *ControllerV1) UserUpdate(ctx context.Context, req *v1.UserUpdateReq) (res *v1.UserUpdateRes, err error) {
	err = service.User().UpdateUser(ctx, &model.UserUpdateInput{
		Uid:     req.Uid,
		RoleIds: req.RoleIds,
		IsAdmin: req.IsAdmin,
	})
	if err != nil {
		return nil, err
	}
	return &v1.UserUpdateRes{Uid: req.Uid}, nil
}
