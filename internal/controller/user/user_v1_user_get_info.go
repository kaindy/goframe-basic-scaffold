package user

import (
	"context"
	"encoding/json"
	"github.com/gogf/gf/v2/util/gconv"
	"goframe-basic-scaffold/api/user/v1"
	"goframe-basic-scaffold/internal/service"
)

type Payload struct {
	Exp      int    `json:"exp"`
	OrigIat  int    `json:"orig_iat"`
	Uid      uint   `json:"uid"`
	Username string `json:"username"`
}

func (c *ControllerV1) UserGetInfo(ctx context.Context, req *v1.UserGetInfoReq) (res *v1.UserGetInfoRes, err error) {
	var payload Payload
	err = json.Unmarshal([]byte(service.Auth().GetPayload(ctx)), &payload)
	if err != nil {
		return nil, err
	}
	return &v1.UserGetInfoRes{
		Id:          gconv.Uint(service.Auth().GetIdentity(ctx)),
		IdentityKey: service.Auth().IdentityKey,
		Payload:     payload,
	}, nil
}
