package user

import (
	"context"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/user/v1"
)

func (c *ControllerV1) UserDelete(ctx context.Context, req *v1.UserDeleteReq) (res *v1.UserDeleteRes, err error) {
	err = service.User().DeleteUser(ctx, req.Uid)
	if err != nil {
		return nil, err
	}
	return &v1.UserDeleteRes{}, nil
}
