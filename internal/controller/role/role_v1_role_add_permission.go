package role

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleAddPermission(ctx context.Context, req *v1.RoleAddPermissionReq) (res *v1.RoleAddPermissionRes, err error) {
	out, err := service.Role().RoleAddPermission(ctx, &model.RoleAddPermissionInput{
		RoleId:       req.RoleId,
		PermissionId: req.PermissionId,
	})
	if err != nil {
		return nil, err
	}
	return &v1.RoleAddPermissionRes{Id: out.Id}, nil
}
