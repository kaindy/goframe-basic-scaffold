package role

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleDeletePermission(ctx context.Context, req *v1.RoleDeletePermissionReq) (res *v1.RoleDeletePermissionRes, err error) {
	err = service.Role().RoleDeletePermission(ctx, &model.RoleDeletePermissionInput{
		RoleId:       req.RoleId,
		PermissionId: req.PermissionId,
	})
	if err != nil {
		return nil, err
	}
	return &v1.RoleDeletePermissionRes{}, nil
}
