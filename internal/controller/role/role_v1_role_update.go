package role

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleUpdate(ctx context.Context, req *v1.RoleUpdateReq) (res *v1.RoleUpdateRes, err error) {
	err = service.Role().RoleUpdate(ctx, &model.RoleUpdateInput{
		Id:   req.Id,
		Name: req.Name,
		Desc: req.Desc,
	})
	if err != nil {
		return nil, err
	}
	return &v1.RoleUpdateRes{Id: req.Id}, nil
}
