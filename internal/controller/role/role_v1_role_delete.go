package role

import (
	"context"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleDelete(ctx context.Context, req *v1.RoleDeleteReq) (res *v1.RoleDeleteRes, err error) {
	err = service.Role().RoleDelete(ctx, req.Id)
	if err != nil {
		return nil, err
	}
	return &v1.RoleDeleteRes{}, nil
}
