package role

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleCreate(ctx context.Context, req *v1.RoleCreateReq) (res *v1.RoleCreateRes, err error) {
	out, err := service.Role().RoleCreate(ctx, &model.RoleCreateInput{
		Name: req.Name,
		Desc: req.Desc,
	})
	if err != nil {
		return nil, err
	}
	return &v1.RoleCreateRes{Id: out.Id}, nil
}
