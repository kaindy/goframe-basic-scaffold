// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package role

import (
	"goframe-basic-scaffold/api/role"
)

type ControllerV1 struct{}

func NewV1() role.IRoleV1 {
	return &ControllerV1{}
}
