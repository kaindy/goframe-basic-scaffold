package role

import (
	"context"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/role/v1"
)

func (c *ControllerV1) RoleGetList(ctx context.Context, req *v1.RoleGetListReq) (res *v1.RoleGetListRes, err error) {
	list, err := service.Role().RoleList(ctx, &model.RoleGetListInput{CommonPaginationInput: model.CommonPaginationInput{
		PageNum:  req.PageNum,
		PageSize: req.PageSize,
	}})
	if err != nil {
		return nil, err
	}
	return &v1.RoleGetListRes{
		Lists: list.Lists,
		PaginationRes: common.PaginationRes{
			PageNum:  list.PageNum,
			PageSize: list.PageSize,
			Total:    list.Total,
		},
	}, err
}
