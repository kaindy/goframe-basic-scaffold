package auth

import (
	"context"
	"github.com/goflyfox/gtoken/gtoken"
	"github.com/gogf/gf/v2/net/ghttp"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/auth/v1"
)

func (c *ControllerV1) AuthLogin(ctx context.Context, req *v1.AuthLoginReq) (res *v1.AuthLoginRes, err error) {
	res = &v1.AuthLoginRes{}
	res.Token, res.Expire = service.Auth().LoginHandler(ctx)
	return
}

func (c *ControllerV1) AuthLoginByGToken(r *ghttp.Request) (string, interface{}) {
	username := r.Get("username").String()
	password := r.Get("password").String()

	if username == "" || password == "" {
		r.Response.WriteJson(gtoken.Fail("账号或密码错误."))
		r.ExitAll()
	}
	// 唯一标识，扩展参数user data
	return username, "1"
}
