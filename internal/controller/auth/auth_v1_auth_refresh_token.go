package auth

import (
	"context"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/auth/v1"
)

func (c *ControllerV1) AuthRefreshToken(ctx context.Context, req *v1.AuthRefreshTokenReq) (res *v1.AuthRefreshTokenRes, err error) {
	res = &v1.AuthRefreshTokenRes{}
	res.Token, res.Expire = service.Auth().RefreshHandler(ctx)
	return
}
