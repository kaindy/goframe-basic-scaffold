package auth

import (
	"context"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/auth/v1"
)

func (c *ControllerV1) AuthLogout(ctx context.Context, req *v1.AuthLogoutReq) (res *v1.AuthLogoutRes, err error) {
	service.Auth().LogoutHandler(ctx)
	return
}
