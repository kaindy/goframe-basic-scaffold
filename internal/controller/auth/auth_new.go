// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package auth

import (
	"goframe-basic-scaffold/api/auth"
)

type ControllerV1 struct{}

func NewV1() auth.IAuthV1 {
	return &ControllerV1{}
}
