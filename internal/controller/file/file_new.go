// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package file

import (
	"goframe-basic-scaffold/api/file"
)

type ControllerV1 struct{}

func NewV1() file.IFileV1 {
	return &ControllerV1{}
}
