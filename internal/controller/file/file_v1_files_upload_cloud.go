package file

import (
	"context"
	v1 "goframe-basic-scaffold/api/file/v1"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"

	"github.com/gogf/gf/v2/errors/gerror"
)

func (c *ControllerV1) FilesUploadCloud(ctx context.Context, req *v1.FilesUploadCloudReq) (res *v1.FilesUploadCloudRes, err error) {
	if req.Files == nil {
		return nil, gerror.NewCode(bizcode.CodeMissingParameter, "没有上传文件")
	}

	// 这里需要对多个文件切片参数进行校验，包含大小和类型，但参数校验在业务中放置在 controller 层
	// 故这里对参数切片进行循环检查，只要有不符合要求的文件，就返回错误
	// TODO

	files, err := service.File().UploadFileToCloud(ctx, &model.FilesUploadCloudInput{
		Files:      req.Files,
		RandomName: req.RandomName,
	})
	if err != nil {
		return nil, err
	}
	return &v1.FilesUploadCloudRes{Files: files}, nil
}
