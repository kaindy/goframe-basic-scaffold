package file

import (
	"context"
	"github.com/gogf/gf/v2/errors/gerror"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"

	"goframe-basic-scaffold/api/file/v1"
)

func (c *ControllerV1) FilesUpload(ctx context.Context, req *v1.FilesUploadReq) (res *v1.FilesUploadRes, err error) {
	if req.Files == nil {
		return nil, gerror.NewCode(bizcode.CodeMissingParameter, "缺失上传文件")
	}

	// 这里需要对多个文件切片参数进行校验，包含大小和类型，但参数校验在业务中放置在 controller 层
	// 故这里对参数切片进行循环检查，只要有不符合要求的文件，就返回错误
	// TODO

	files, err := service.File().MultipartUpload(ctx, &model.FilesUploadInput{
		Files:      req.Files,
		RandomName: req.RandomName,
	})
	if err != nil {
		return nil, err
	}

	return &v1.FilesUploadRes{Files: files}, nil
}
