package file

import (
	"context"
	"github.com/gogf/gf/v2/errors/gerror"
	"goframe-basic-scaffold/api/file/v1"
	"goframe-basic-scaffold/internal/controller/common"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"
)

func (c *ControllerV1) FileUpload(ctx context.Context, req *v1.FileUploadReq) (res *v1.FileUploadRes, err error) {
	if req.File == nil {
		return nil, gerror.NewCode(bizcode.CodeMissingParameter, "缺少上传文件")
	}

	// 检查文件大小和类型是否符合配置设定
	err = common.VerifyUploadFileSize(ctx, req.File)
	if err != nil {
		return nil, err
	}

	// 检查文件类型
	err = common.VerifyUploadFileType(ctx, req.File)
	if err != nil {
		return nil, err
	}

	file, err := service.File().Upload(ctx, &model.FileUploadInput{
		File:       req.File,
		Name:       req.Name,
		RandomName: req.RandomName,
	})
	if err != nil {
		return nil, err
	}
	return &v1.FileUploadRes{
		Name: file.Name,
		Url:  file.Url,
	}, nil
}
