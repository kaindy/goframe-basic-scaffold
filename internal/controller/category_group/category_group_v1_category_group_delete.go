package category_group

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/category_group/v1"
)

func (c *ControllerV1) CategoryGroupDelete(ctx context.Context, req *v1.CategoryGroupDeleteReq) (res *v1.CategoryGroupDeleteRes, err error) {
	err = service.CategoryGroup().CategoryGroupDelete(ctx, &model.CategoryGroupDeleteInput{CategoryGroupId: req.CategoryGroupId})
	if err != nil {
		return nil, err
	}
	return &v1.CategoryGroupDeleteRes{}, err
}
