// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package category_group

import (
	"goframe-basic-scaffold/api/category_group"
)

type ControllerV1 struct{}

func NewV1() category_group.ICategoryGroupV1 {
	return &ControllerV1{}
}
