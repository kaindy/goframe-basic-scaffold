package category_group

import (
	"context"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/category_group/v1"
)

func (c *ControllerV1) CategoryGroupGetList(ctx context.Context, req *v1.CategoryGroupGetListReq) (res *v1.CategoryGroupGetListRes, err error) {
	list, err := service.CategoryGroup().CategoryGroupGetList(ctx, &model.CategoryGroupGetListInput{
		CommonPaginationInput: model.CommonPaginationInput{
			PageNum:  req.PageNum,
			PageSize: req.PageSize,
		},
	})
	if err != nil {
		return nil, err
	}
	return &v1.CategoryGroupGetListRes{
		Lists: list.Lists,
		PaginationRes: common.PaginationRes{
			PageNum:  list.PageNum,
			PageSize: list.PageSize,
			Total:    list.Total,
		},
	}, nil
}
