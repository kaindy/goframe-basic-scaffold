package category_group

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"

	"github.com/gogf/gf/v2/errors/gerror"

	"goframe-basic-scaffold/api/category_group/v1"
)

func (c *ControllerV1) CategoryGroupCreate(ctx context.Context, req *v1.CategoryGroupCreateReq) (res *v1.CategoryGroupCreateRes, err error) {
	if req.CategoryGroupName == "" {
		return nil, gerror.NewCode(bizcode.CodeInvalidParameter, "缺少分类组名称")
	}
	out, err := service.CategoryGroup().CategoryGroupCreate(ctx, &model.CategoryGroupCreateInput{
		CategoryGroupName:        req.CategoryGroupName,
		CategoryGroupDescription: req.CategoryGroupDescription,
	})
	if err != nil {
		return nil, err
	}

	return &v1.CategoryGroupCreateRes{CategoryGroupId: out.CategoryGroupId}, nil
}
