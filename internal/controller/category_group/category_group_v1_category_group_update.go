package category_group

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/service"

	"goframe-basic-scaffold/api/category_group/v1"
)

func (c *ControllerV1) CategoryGroupUpdate(ctx context.Context, req *v1.CategoryGroupUpdateReq) (res *v1.CategoryGroupUpdateRes, err error) {
	err = service.CategoryGroup().CategoryGroupUpdate(ctx, &model.CategoryGroupUpdateInput{
		CategoryGroupId:          req.CategoryGroupId,
		CategoryGroupName:        req.CategoryGroupName,
		CategoryGroupDescription: req.CategoryGroupDescription,
	})
	if err != nil {
		return nil, err
	}
	return &v1.CategoryGroupUpdateRes{CategoryGroupId: req.CategoryGroupId}, nil
}
