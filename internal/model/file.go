package model

import "github.com/gogf/gf/v2/net/ghttp"

type FileUploadInput struct {
	File       *ghttp.UploadFile `json:"file"`        // 文件上传对象
	Name       string            `json:"name"`        // 文件名
	RandomName bool              `json:"random_name"` // 是否随机重命名
}

type FileUploadOutput struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Src  string `json:"src"`
	Url  string `json:"url"`
}

type FilesUploadInput struct {
	Files      *ghttp.UploadFiles `json:"files"`
	RandomName bool               `json:"randomName"`
}

type FilesUploadOutput struct {
	Files []*FileUploadOutput
}

type FilesUploadCloudInput struct {
	Files      *ghttp.UploadFiles `json:"files"`
	RandomName bool               `json:"random_name"`
}

type FilesUploadCloudOutput struct {
	Files []*FileUploadOutput
}
