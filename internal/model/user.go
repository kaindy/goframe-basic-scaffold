package model

import (
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model/entity"
)

type UserLoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserLoginOutput struct {
	Id       uint   `json:"id"`
	Username string `json:"username"`
	RoleIds  string `json:"role_ids"`
	IsAdmin  int    `json:"is_admin"`
	CommonDatetimeOutput
}

type UserLoginByGTokenOutput struct {
	Type        string                  `json:"type"`
	Token       string                  `json:"token"`
	ExpireIn    int                     `json:"expire_in"`
	Uid         uint                    `json:"uid"`
	Username    string                  `json:"username"`
	IsAdmin     int                     `json:"is_admin"`    //是否超管
	RoleIds     string                  `json:"role_ids"`    //角色
	Permissions []entity.PermissionInfo `json:"permissions"` //权限列表
}

type UserCreateInput struct {
	Uid      string `json:"uid"`
	Name     string `json:"name"`
	Password string `json:"password"`
	UserSalt string `json:"user_salt"`
	RoleIds  string `json:"role_ids"`
	IsAdmin  int8   `json:"is_admin"`
}

type UserCreateOutput struct {
	Uid string `json:"uid"`
}

type UserDeleteInput struct {
	Id uint `json:"id"`
}

type UserDeleteOutput struct{}

type UserUpdateInput struct {
	Uid     string `json:"uid"`
	RoleIds string `json:"role_ids"`
	IsAdmin int8   `json:"is_admin"`
}

type UserUpdateOutput struct {
	Id uint `json:"id"`
}

type UserGetListInput struct {
	CommonPaginationInput
}

type UserGetListOutput struct {
	Lists []*UserGetListOutputItem `json:"lists"`
	common.PaginationRes
}

type UserGetListOutputItem struct {
	Id      uint   `json:"id"`
	Uid     string `json:"uid"`
	Name    string `json:"name"`
	RoleIds string `json:"role_ids"`
	IsAdmin int8   `json:"is_admin"`
	CommonDatetimeOutput
}

type UserUpdatePasswordInput struct {
	Uid         string `json:"uid"`
	OldPassword string `json:"old_password"`
	Password    string `json:"password"`
	RePassword  string `json:"re_password"`
	UserSalt    string `json:"user_salt"`
}

type UserUpdatePasswordOutput struct {
}
