package model

type RoleCreateInput struct {
	Name string `json:"name"`
	Desc string `json:"desc"`
}

type RoleCreateOutput struct {
	Id uint `json:"id"`
}

type RoleUpdateInput struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Desc string `json:"desc"`
}

type RoleUpdateOutput struct {
	Id uint `json:"id"`
}

type RoleGetListInput struct {
	CommonPaginationInput
}

type RoleGetListOutput struct {
	Lists []*RoleGetListOutputItem `json:"lists"`
	CommonPaginationOutput
}

type RoleGetListOutputItem struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Desc string `json:"desc"`
	CommonDatetimeOutput
}

type RoleAddPermissionInput struct {
	RoleId       uint `json:"role_id"`
	PermissionId uint `json:"permission_id"`
}

type RoleAddPermissionOutput struct {
	Id uint `json:"id"`
}

type RoleDeletePermissionInput struct {
	RoleId       uint `json:"role_id"`
	PermissionId uint `json:"permission_id"`
}

type RoleDeletePermissionOutput struct{}
