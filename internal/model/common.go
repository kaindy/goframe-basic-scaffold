package model

import "github.com/gogf/gf/v2/os/gtime"

type CommonPaginationInput struct {
	PageNum  int `json:"page_num" dc:"分页码"`
	PageSize int `json:"page_size" dc:"分页数量"`
}

type CommonPaginationOutput struct {
	PageNum  int `json:"page_num" dc:"分页码"`
	PageSize int `json:"page_size" dc:"分页数量"`
	Total    int `json:"total" dc:"数据总量"`
}

type CommonDatetimeOutput struct {
	CreatedAt *gtime.Time `json:"created_at" dc:"资源创建日期时间"`
	UpdatedAt *gtime.Time `json:"updated_at" dc:"资源最后更新日期时间"`
}
