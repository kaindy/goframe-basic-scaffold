// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// User is the golang structure of table user for DAO operations like Where/Data.
type User struct {
	g.Meta    `orm:"table:user, do:true"`
	Id        interface{} // 记录ID
	Uid       interface{} // 用户ID
	Username  interface{} // 用户名
	Password  interface{} // 用户密码
	UserSalt  interface{} // 用户加密盐
	Realname  interface{} // 真实姓名
	Nickname  interface{} // 昵称
	Phone     interface{} // 手机号码
	CreatedAt *gtime.Time // 创建日期时间
	UpdatedAt *gtime.Time // 更新日期时间
	DeletedAt *gtime.Time // 删除日期时间
}
