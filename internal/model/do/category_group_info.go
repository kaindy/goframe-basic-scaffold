// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// CategoryGroupInfo is the golang structure of table category_group_info for DAO operations like Where/Data.
type CategoryGroupInfo struct {
	g.Meta                   `orm:"table:category_group_info, do:true"`
	Id                       interface{} //
	CategoryGroupId          interface{} //
	CategoryGroupName        interface{} //
	CategoryGroupDescription interface{} //
	CreatedAt                *gtime.Time //
	UpdatedAt                *gtime.Time //
	DeletedAt                *gtime.Time //
}
