package model

type PermissionCreateInput struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

type PermissionCreateOutput struct {
	Id uint `json:"id"`
}

type PermissionUpdateInput struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Path string `json:"path"`
}

type PermissionUpdateOutput struct {
	Id uint `json:"id"`
}

type PermissionGetListInput struct {
	CommonPaginationInput
}

type PermissionGetListOutput struct {
	Lists []*PermissionListItem `json:"lists"`
	CommonPaginationOutput
}

type PermissionListItem struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Path string `json:"path"`
	CommonDatetimeOutput
}
