package model

type CategoryGroupCreateInput struct {
	CategoryGroupId          string `json:"category_group_id"`
	CategoryGroupName        string `json:"category_group_name"`
	CategoryGroupDescription string `json:"category_group_description"`
}

type CategoryGroupCreateOutput struct {
	CategoryGroupId string `json:"category_group_id"`
}

type CategoryGroupDeleteInput struct {
	CategoryGroupId string `json:"category_group_id"`
}

type CategoryGroupDeleteOutput struct{}

type CategoryGroupUpdateInput struct {
	CategoryGroupId          string `json:"category_group_id"`
	CategoryGroupName        string `json:"category_group_name"`
	CategoryGroupDescription string `json:"category_group_description"`
}

type CategoryGroupUpdateOutput struct {
	CategoryGroupId string `json:"category_group_id"`
}

type CategoryGroupGetListInput struct {
	CommonPaginationInput
}

type CategoryGroupGetListOutput struct {
	Lists []*CategoryGroupItem `json:"lists"`
	CommonPaginationOutput
}

type CategoryGroupItem struct {
	Id                       uint   `json:"id" dc:"ID"`
	CategoryGroupId          string `json:"category_group_id" dc:"分类组ID"`
	CategoryGroupName        string `json:"category_group_name" dc:"分类组名称"`
	CategoryGroupDescription string `json:"category_group_description" dc:"分类组描述"`
	CommonDatetimeOutput
}
