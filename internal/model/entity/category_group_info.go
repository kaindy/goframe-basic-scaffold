// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// CategoryGroupInfo is the golang structure for table category_group_info.
type CategoryGroupInfo struct {
	Id                       int         `json:"id"                       description:""`
	CategoryGroupId          string      `json:"categoryGroupId"          description:""`
	CategoryGroupName        string      `json:"categoryGroupName"        description:""`
	CategoryGroupDescription string      `json:"categoryGroupDescription" description:""`
	CreatedAt                *gtime.Time `json:"createdAt"                description:""`
	UpdatedAt                *gtime.Time `json:"updatedAt"                description:""`
	DeletedAt                *gtime.Time `json:"deletedAt"                description:""`
}
