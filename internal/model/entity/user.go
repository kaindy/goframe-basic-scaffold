// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// User is the golang structure for table user.
type User struct {
	Id        int         `json:"id"        description:"记录ID"`
	Uid       string      `json:"uid"       description:"用户ID"`
	Username  string      `json:"username"  description:"用户名"`
	Password  string      `json:"password"  description:"用户密码"`
	UserSalt  string      `json:"userSalt"  description:"用户加密盐"`
	Realname  string      `json:"realname"  description:"真实姓名"`
	Nickname  string      `json:"nickname"  description:"昵称"`
	Phone     string      `json:"phone"     description:"手机号码"`
	CreatedAt *gtime.Time `json:"createdAt" description:"创建日期时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" description:"更新日期时间"`
	DeletedAt *gtime.Time `json:"deletedAt" description:"删除日期时间"`
}
