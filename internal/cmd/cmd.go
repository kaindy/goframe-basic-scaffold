package cmd

import (
	"context"
	"goframe-basic-scaffold/internal/controller/category_group"
	"goframe-basic-scaffold/internal/controller/file"
	"goframe-basic-scaffold/internal/controller/permission"
	"goframe-basic-scaffold/internal/controller/role"
	"goframe-basic-scaffold/internal/controller/user"
	"goframe-basic-scaffold/internal/service"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcmd"
)

var (
	Main = gcmd.Command{
		Name:  "goframe-basic-scaffold",
		Usage: "main",
		Brief: "start http server",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			s := g.Server()
			// 路由组定义，无需鉴权
			s.Group("/", func(group *ghttp.RouterGroup) {
				// 组中间件绑定
				group.Middleware(
					service.Middleware().CORS,
					ghttp.MiddlewareHandlerResponse,
				)
				group.ALLMap(g.Map{})
			})
			// 需要鉴权的路由组定义.
			s.Group("/", func(group *ghttp.RouterGroup) {
				group.Middleware(
					service.Middleware().CORS,
					ghttp.MiddlewareHandlerResponse,
				)
				// 读取配置，根据 gToken.enable 的值来判断是否启用 gToken
				if g.Cfg().MustGet(ctx, "gToken.enable").Bool() {
					err = service.Middleware().GTokenHandler(ctx).Middleware(ctx, group)
					if err != nil {
						return
					}
				}
				group.ALLMap(g.Map{
					// 用户相关
					"/user/create":        user.NewV1().UserCreate,
					"/user/delete":        user.NewV1().UserDelete,
					"/user/update":        user.NewV1().UserUpdate,
					"/user/list":          user.NewV1().UserGetList,
					"/user/update_passwd": user.NewV1().UserUpdatePassword,
					// 角色相关
					"/role/create":            role.NewV1().RoleCreate,
					"/role/delete":            role.NewV1().RoleDelete,
					"/role/update":            role.NewV1().RoleUpdate,
					"/role/list":              role.NewV1().RoleGetList,
					"/role/add/permission":    role.NewV1().RoleAddPermission,
					"/role/delete/permission": role.NewV1().RoleDeletePermission,
					// 权限相关
					"/permission/create": permission.NewV1().PermissionCreate,
					"/permission/delete": permission.NewV1().PermissionDelete,
					"/permission/update": permission.NewV1().PermissionUpdate,
					"permission/list":    permission.NewV1().PermissionGetList,
					// 文件相关
					"/file/upload":        file.NewV1().FileUpload,
					"/files/upload":       file.NewV1().FilesUpload,
					"/files/upload_cloud": file.NewV1().FilesUploadCloud,
					// 分类组相关
					"/category_group/create": category_group.NewV1().CategoryGroupCreate,
					"/category_group/delete": category_group.NewV1().CategoryGroupDelete,
					"/category_group/update": category_group.NewV1().CategoryGroupUpdate,
					"/category_group/list":   category_group.NewV1().CategoryGroupGetList,
				})
			})
			s.Run()
			return nil
		},
	}
)
