package packed

import (
	"github.com/gogf/gf/v2/frame/g"
	"goframe-basic-scaffold/utility/snowflake"
)

func init() {
	// 初始化雪花ID生成功能
	err := snowflake.InitSFInstance("2023-01-01", 1)
	if err != nil {
		panic(err)
	}
	g.Dump("init snowflake instance success..")
}
