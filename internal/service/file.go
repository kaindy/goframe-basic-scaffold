// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"goframe-basic-scaffold/internal/model"
)

type (
	IFile interface {
		// Upload 上传单个文件到本地
		Upload(ctx context.Context, in *model.FileUploadInput) (out *model.FileUploadOutput, err error)
		// MultipartUpload 上传多个文件到本地
		MultipartUpload(ctx context.Context, in *model.FilesUploadInput) (out []*model.FileUploadOutput, err error)
		// UploadFileToCloud 上传文件到SSO存储空间，支持单个或多个文件
		UploadFileToCloud(ctx context.Context, in *model.FilesUploadCloudInput) (out []*model.FileUploadOutput, err error)
	}
)

var (
	localFile IFile
)

func File() IFile {
	if localFile == nil {
		panic("implement not found for interface IFile, forgot register?")
	}
	return localFile
}

func RegisterFile(i IFile) {
	localFile = i
}
