// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"goframe-basic-scaffold/internal/model"
)

type (
	ICategoryGroup interface {
		// CategoryGroupCreate 创建分类组
		CategoryGroupCreate(ctx context.Context, in *model.CategoryGroupCreateInput) (out *model.CategoryGroupCreateOutput, err error)
		// CategoryGroupDelete 删除分类组
		CategoryGroupDelete(ctx context.Context, in *model.CategoryGroupDeleteInput) error
		// CategoryGroupUpdate 更新分组类
		CategoryGroupUpdate(ctx context.Context, in *model.CategoryGroupUpdateInput) error
		// CategoryGroupGetList 获取分类组列表
		CategoryGroupGetList(ctx context.Context, in *model.CategoryGroupGetListInput) (out *model.CategoryGroupGetListOutput, err error)
	}
)

var (
	localCategoryGroup ICategoryGroup
)

func CategoryGroup() ICategoryGroup {
	if localCategoryGroup == nil {
		panic("implement not found for interface ICategoryGroup, forgot register?")
	}
	return localCategoryGroup
}

func RegisterCategoryGroup(i ICategoryGroup) {
	localCategoryGroup = i
}
