// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"goframe-basic-scaffold/internal/model"
)

type (
	IRole interface {
		RoleCreate(ctx context.Context, in *model.RoleCreateInput) (out *model.RoleCreateOutput, err error)
		RoleDelete(ctx context.Context, id uint) error
		RoleUpdate(ctx context.Context, in *model.RoleUpdateInput) error
		RoleList(ctx context.Context, in *model.RoleGetListInput) (out *model.RoleGetListOutput, err error)
		RoleAddPermission(ctx context.Context, in *model.RoleAddPermissionInput) (out *model.RoleAddPermissionOutput, err error)
		RoleDeletePermission(ctx context.Context, in *model.RoleDeletePermissionInput) error
	}
)

var (
	localRole IRole
)

func Role() IRole {
	if localRole == nil {
		panic("implement not found for interface IRole, forgot register?")
	}
	return localRole
}

func RegisterRole(i IRole) {
	localRole = i
}
