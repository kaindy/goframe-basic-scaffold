package service

import (
	"context"
	"goframe-basic-scaffold/utility/bizcode"
	"goframe-basic-scaffold/utility/response"

	"github.com/goflyfox/gtoken/gtoken"
	"github.com/gogf/gf/v2/frame/g"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/net/ghttp"
)

type sMiddleware struct{}

var middleware = sMiddleware{}

func Middleware() *sMiddleware {
	return &middleware
}

// ResponseHandler 返回处理中间件
func (s *sMiddleware) ResponseHandler(r *ghttp.Request) {
	r.Middleware.Next()

	// 如果已经有返回内容，那么中间件什么都不做
	if r.Response.BufferLength() > 0 {
		return
	}

	var (
		err             = r.GetError()
		res             = r.GetHandlerResponse()
		code gcode.Code = bizcode.CodeOK
	)

	if err != nil {
		code = gerror.Code(err)
		if code == bizcode.CodeNil {
			code = bizcode.CodeInternalError
		}
		response.JsonExit(r, code.Code(), err.Error())
	} else {
		response.JsonExit(r, code.Code(), "", res)
	}

}

func (s *sMiddleware) CORS(r *ghttp.Request) {
	r.Response.CORSDefault()
	r.Middleware.Next()
}

// Auth 基于 gf-jwt 的认证实现
func (s *sMiddleware) Auth(r *ghttp.Request) {
	Auth().MiddlewareFunc()(r)
	r.Middleware.Next()
}

// GTokenHandler 基于 gToken 的身份验证实现
func (s *sMiddleware) GTokenHandler(ctx context.Context) *gtoken.GfToken {
	return &gtoken.GfToken{
		ServerName:       g.Cfg().MustGet(ctx, "gToken.ServerName").String(),
		CacheMode:        g.Cfg().MustGet(ctx, "gToken.CacheMode").Int8(),
		CacheKey:         g.Cfg().MustGet(ctx, "gToken.CacheKey").String(),
		Timeout:          g.Cfg().MustGet(ctx, "gToken.Timeout").Int(),
		MaxRefresh:       g.Cfg().MustGet(ctx, "gToken.MaxRefresh").Int(),
		TokenDelimiter:   g.Cfg().MustGet(ctx, "gToken.TokenDelimiter").String(),
		EncryptKey:       g.Cfg().MustGet(ctx, "gToken.EncryptKey").Bytes(),
		AuthFailMsg:      g.Cfg().MustGet(ctx, "gToken.AuthFailMsg").String(),
		MultiLogin:       g.Cfg().MustGet(ctx, "gToken.MultiLogin").Bool(), // 是否支持多端登录
		LoginPath:        "/auth/login",
		LoginBeforeFunc:  AuthLoginBeforeFuncByGToken,
		LoginAfterFunc:   AuthLoginAfterFuncByGToken,
		LogoutPath:       "/auth/logout",
		LogoutBeforeFunc: AuthLogoutBeforeFunc,
		LogoutAfterFunc:  AuthLogoutAfterFunc,
		AuthBeforeFunc:   AuthBeforeFunc,
		AuthAfterFunc:    AuthAfterFunc,
		// GlobalMiddleware: false, // 开启全局拦截
	}
}
