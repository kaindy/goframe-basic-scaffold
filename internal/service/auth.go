package service

import (
	"context"
	"goframe-basic-scaffold/internal/model"
	"time"

	jwt "github.com/gogf/gf-jwt/v2"
	"github.com/gogf/gf/v2/frame/g"
)

var authService *jwt.GfJWTMiddleware

func Auth() *jwt.GfJWTMiddleware {
	return authService
}

func init() {
	auth := jwt.New(&jwt.GfJWTMiddleware{
		Realm:           "goframe-shop",
		Key:             []byte("secret key"),
		Timeout:         time.Minute * 60 * 24,
		MaxRefresh:      time.Minute * 60 * 12,
		IdentityKey:     "uid",
		TokenLookup:     "header: Authorization, query: token, cookie: jwt",
		TokenHeadName:   "Bearer",
		TimeFunc:        time.Now,
		Authenticator:   Authenticator,
		Unauthorized:    Unauthorized,
		PayloadFunc:     PayloadFunc,
		IdentityHandler: IdentityHandler,
	})
	authService = auth
}

// PayloadFunc PayloadFunc是一个回调函数，在登录时会被调用。
// 使用此函数可以向 web token 添加额外的有效载荷数据。
// 然后，可以通过c.Get（"JWT_PAYLOAD"）在请求期间访问该数据。
// 请注意，该数据未加密。
// 不能将jwt.io上提到的属性用作map的键。
// 可选，默认情况下不会设置任何其他数据
func PayloadFunc(data interface{}) jwt.MapClaims {
	claims := jwt.MapClaims{}
	params := data.(map[string]interface{})
	if len(params) > 0 {
		for k, v := range params {
			claims[k] = v
		}
	}
	return claims
}

// IdentityHandler 从JWT中获取身份，并为每个请求设置身份
// 使用此函数，通过r.GetParam("id")获取身份
func IdentityHandler(ctx context.Context) interface{} {
	claims := jwt.ExtractClaims(ctx)
	return claims[authService.IdentityKey]
}

// Unauthorized 是用来定义自定义的未授权时的回调函数
func Unauthorized(ctx context.Context, code int, message string) {
	r := g.RequestFromCtx(ctx)
	r.Response.WriteJson(g.Map{
		"code":    code,
		"message": message,
	})
	r.ExitAll()
}

// Authenticator 用于验证登录参数。
// 它必须返回用户数据作为用户标识符，它将被存储在Claim数组中。
// 如果您的identityKey是'id'，则您的用户数据必须具有'id'
// 检查错误（e）以确定适当的错误消息。
func Authenticator(ctx context.Context) (interface{}, error) {
	var (
		r  = g.RequestFromCtx(ctx)
		in model.UserLoginInput
	)
	if err := r.Parse(&in); err != nil {
		return "", err
	}

	if user, _ := User().GetUserByNamePassword(ctx, in); user != nil {
		return user, nil
	}

	return nil, jwt.ErrFailedAuthentication
}
