package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
	"goframe-basic-scaffold/internal/consts"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/utility"
	"goframe-basic-scaffold/utility/bizcode"
	"goframe-basic-scaffold/utility/response"
	"net/http"
	"strconv"

	"github.com/goflyfox/gtoken/gtoken"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

func AuthLoginBeforeFuncByGToken(r *ghttp.Request) (string, interface{}) {
	username := r.Get("username").String()
	password := r.Get("password").String()
	if username == "" || password == "" {
		r.Response.WriteJson(gtoken.Fail("账号或密码错误."))
		r.ExitAll()
	}

	ctx := context.TODO()
	userInfo := &entity.AdminInfo{}
	// 检查用户是否存在
	err := dao.AdminInfo.Ctx(ctx).Where(dao.AdminInfo.Columns().Name, username).Scan(&userInfo)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		r.Response.WriteJson(gtoken.Fail("账号不存在."))
		r.ExitAll()
	}
	// 检查用户密码是否正确
	if utility.EncryptPassword(password, userInfo.UserSalt) != userInfo.Password {
		r.Response.WriteJson(gtoken.Fail("账号或密码错误."))
		r.ExitAll()
	}
	// 唯一标识，扩展参数user data
	return strconv.Itoa(userInfo.Id), userInfo
}

func AuthLoginAfterFuncByGToken(r *ghttp.Request, respData gtoken.Resp) {
	if !respData.Success() {
		respData.Code = 0
		r.Response.WriteJson(respData)
		return
	} else {
		respData.Code = 200
		// 获取登录用户ID
		userKey := respData.GetString("userKey")
		// 根据ID获取用户其他信息
		userInfo := &entity.AdminInfo{}
		err := dao.AdminInfo.Ctx(context.TODO()).WherePri(userKey).Scan(&userInfo)
		if err != nil {
			return
		}
		// 查询权限
		var rolePermissionInfos []entity.RolePermissionInfo
		err = dao.RolePermissionInfo.Ctx(context.TODO()).WhereIn(dao.RolePermissionInfo.Columns().RoleId, g.Slice{userInfo.RoleIds}).Scan(&rolePermissionInfos)
		if err != nil {
			return
		}
		permissionIds := g.Slice{}
		for _, info := range rolePermissionInfos {
			permissionIds = append(permissionIds, info.PermissionId)
		}
		// 通过权限ID列表查询权限
		var permissions []entity.PermissionInfo
		err = dao.PermissionInfo.Ctx(context.TODO()).WhereIn(dao.PermissionInfo.Columns().Id, permissionIds).Scan(&permissions)
		if err != nil {
			return
		}
		response.JsonExit(r, 0, "", &model.UserLoginByGTokenOutput{
			Type:        consts.TokenType,
			Token:       respData.GetString("token"),
			ExpireIn:    consts.GTokenExpireIn, // 单位：秒
			Uid:         uint(userInfo.Id),
			Username:    userInfo.Name,
			IsAdmin:     userInfo.IsAdmin,
			RoleIds:     userInfo.RoleIds,
			Permissions: nil,
		})
	}
}

func AuthLogoutBeforeFunc(r *ghttp.Request) bool {
	return true
}

func AuthLogoutAfterFunc(r *ghttp.Request, respData gtoken.Resp) {
	return
}

func AuthBeforeFunc(r *ghttp.Request) bool {
	return true
}

// AuthAfterFunc 验证后的回调处理函数
// 自定义了返回的数据格式
func AuthAfterFunc(r *ghttp.Request, respData gtoken.Resp) {
	if respData.Success() {
		r.Middleware.Next()
	} else {
		var params map[string]interface{}
		if r.Method == http.MethodGet {
			params = r.GetMap()
		} else if r.Method == http.MethodPost {
			params = r.GetMap()
		} else {
			r.Response.Writeln(gtoken.MsgErrReqMethod)
			return
		}

		no := gconv.String(gtime.TimestampMilli())

		g.Log().Warning(r.Context(), fmt.Sprintf("[AUTH_%s][url:%s][params:%s][data:%s]",
			no, r.URL.Path, params, respData.Json()))
		respData.Code = bizcode.CodeNotAuthorized.Code()
		respData.Msg = g.Cfg().MustGet(context.TODO(), "gToken.AuthFailMsg").String()
		respData.Data = nil
		r.Response.WriteJson(respData)
		r.ExitAll()
	}
}
