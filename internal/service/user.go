// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"goframe-basic-scaffold/internal/model"
)

type (
	IUser interface {
		// GetUserByNamePassword 通过账号密码获取用户信息
		GetUserByNamePassword(ctx context.Context, in model.UserLoginInput) (map[string]interface{}, error)
		// CreateUser 创建新用户
		CreateUser(ctx context.Context, in *model.UserCreateInput) (out *model.UserCreateOutput, err error)
		// DeleteUser 删除用户
		DeleteUser(ctx context.Context, uid string) error
		// UpdateUser 修改用户
		UpdateUser(ctx context.Context, in *model.UserUpdateInput) error
		// GetUserList 获取用户列表
		GetUserList(ctx context.Context, in *model.UserGetListInput) (out *model.UserGetListOutput, err error)
		// UpdateUserPassword 更新用户密码
		UpdateUserPassword(ctx context.Context, in *model.UserUpdatePasswordInput) error
	}
)

var (
	localUser IUser
)

func User() IUser {
	if localUser == nil {
		panic("implement not found for interface IUser, forgot register?")
	}
	return localUser
}

func RegisterUser(i IUser) {
	localUser = i
}
