// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// CategoryGroupInfoDao is the data access object for table category_group_info.
type CategoryGroupInfoDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns CategoryGroupInfoColumns // columns contains all the column names of Table for convenient usage.
}

// CategoryGroupInfoColumns defines and stores column names for table category_group_info.
type CategoryGroupInfoColumns struct {
	Id                       string //
	CategoryGroupId          string //
	CategoryGroupName        string //
	CategoryGroupDescription string //
	CreatedAt                string //
	UpdatedAt                string //
	DeletedAt                string //
}

// categoryGroupInfoColumns holds the columns for table category_group_info.
var categoryGroupInfoColumns = CategoryGroupInfoColumns{
	Id:                       "id",
	CategoryGroupId:          "category_group_id",
	CategoryGroupName:        "category_group_name",
	CategoryGroupDescription: "category_group_description",
	CreatedAt:                "created_at",
	UpdatedAt:                "updated_at",
	DeletedAt:                "deleted_at",
}

// NewCategoryGroupInfoDao creates and returns a new DAO object for table data access.
func NewCategoryGroupInfoDao() *CategoryGroupInfoDao {
	return &CategoryGroupInfoDao{
		group:   "default",
		table:   "category_group_info",
		columns: categoryGroupInfoColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *CategoryGroupInfoDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *CategoryGroupInfoDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *CategoryGroupInfoDao) Columns() CategoryGroupInfoColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *CategoryGroupInfoDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *CategoryGroupInfoDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *CategoryGroupInfoDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
