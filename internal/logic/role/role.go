package role

import (
	"context"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

type sRole struct{}

func init() {
	service.RegisterRole(New())
}

func New() *sRole {
	return &sRole{}
}

func (s *sRole) RoleCreate(ctx context.Context, in *model.RoleCreateInput) (out *model.RoleCreateOutput, err error) {
	// 查询当前角色名是否已存在
	count, err := dao.RoleInfo.Ctx(ctx).Where("name", in.Name).Count()
	if err != nil {
		return nil, err
	}
	if count > 0 {
		return nil, gerror.NewCode(bizcode.CodeBusinessValidationFailed, "已存在同名的角色")
	}
	// 插入数据
	lastInsertId, err := dao.RoleInfo.Ctx(ctx).Data(&in).InsertAndGetId()
	if err != nil {
		return nil, err
	}
	return &model.RoleCreateOutput{Id: uint(lastInsertId)}, nil
}

func (s *sRole) RoleDelete(ctx context.Context, id uint) error {
	// 查询当前角色ID是否存在
	count, err := dao.RoleInfo.Ctx(ctx).WherePri(id).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "不存在的角色ID")
	}
	// 执行删除
	return dao.RoleInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.RoleInfo.Ctx(ctx).Where(dao.RoleInfo.Columns().Id, id).Delete()
		if err != nil {
			return err
		}
		return err
	})
}

func (s *sRole) RoleUpdate(ctx context.Context, in *model.RoleUpdateInput) error {
	// 检查角色是否存在
	err := _checkRoleExistsById(ctx, in.Id)
	if err != nil {
		return err
	}
	// 更新角色
	return dao.RoleInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.RoleInfo.Ctx(ctx).
			OmitEmpty().
			Data(&in).
			FieldsEx(dao.RoleInfo.Columns().Id).
			Where(dao.RoleInfo.Columns().Id, in.Id).
			Update()
		if err != nil {
			return err
		}
		return err
	})
}

func (s *sRole) RoleList(ctx context.Context, in *model.RoleGetListInput) (out *model.RoleGetListOutput, err error) {
	var m = dao.RoleInfo.Ctx(ctx)
	out = &model.RoleGetListOutput{
		CommonPaginationOutput: model.CommonPaginationOutput{
			PageNum:  in.PageNum,
			PageSize: in.PageSize,
		},
	}
	// 分页查询，按创建时间倒序排列
	listModel := m.Page(in.PageNum, in.PageSize).OrderDesc(dao.RoleInfo.Columns().CreatedAt)
	// 执行查询
	var list []*entity.RoleInfo
	err = listModel.Scan(&list)
	if err != nil {
		return nil, err
	}
	// 没有数据
	if len(list) == 0 {
		return out, nil
	}
	// 统计数量
	out.Total, err = m.Count()
	if err != nil {
		return out, err
	}
	err = listModel.Scan(&out.Lists)
	if err != nil {
		return nil, err
	}
	return
}

func (s *sRole) RoleAddPermission(ctx context.Context, in *model.RoleAddPermissionInput) (out *model.RoleAddPermissionOutput, err error) {
	lastInsertId, err := dao.RolePermissionInfo.Ctx(ctx).Data(&in).InsertAndGetId()
	if err != nil {
		return nil, err
	}
	return &model.RoleAddPermissionOutput{Id: uint(lastInsertId)}, nil
}

func (s *sRole) RoleDeletePermission(ctx context.Context, in *model.RoleDeletePermissionInput) error {
	return dao.RolePermissionInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err := dao.RolePermissionInfo.Ctx(ctx).Where(g.Map{
			dao.RolePermissionInfo.Columns().RoleId:       in.RoleId,
			dao.RolePermissionInfo.Columns().PermissionId: in.PermissionId,
		}).Delete()
		if err != nil {
			return err
		}
		return err
	})
}

// _checkRoleExistsById 检查角色是否存在
func _checkRoleExistsById(ctx context.Context, id uint) error {
	count, err := dao.RoleInfo.Ctx(ctx).WherePri(id).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "不存在的角色")
	}
	return nil
}
