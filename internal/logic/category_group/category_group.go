package category_group

import (
	"context"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"
	"goframe-basic-scaffold/utility/snowflake"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
)

type sCategoryGroup struct{}

func init() {
	service.RegisterCategoryGroup(New())
}

func New() *sCategoryGroup {
	return &sCategoryGroup{}
}

// CategoryGroupCreate 创建分类组
func (s *sCategoryGroup) CategoryGroupCreate(ctx context.Context, in *model.CategoryGroupCreateInput) (out *model.CategoryGroupCreateOutput, err error) {
	// 检查分类组名称是否重复
	count, err := dao.CategoryGroupInfo.Ctx(ctx).Where(dao.CategoryGroupInfo.Columns().CategoryGroupName, in.CategoryGroupName).Count()
	if err != nil {
		return nil, err
	}
	if count > 0 {
		return nil, gerror.NewCode(bizcode.CodeBusinessValidationFailed, "已存在同名的分类组")
	}
	// 创建分类组ID
	in.CategoryGroupId = snowflake.GenerateSFID()
	// 写入数据
	_, err = dao.CategoryGroupInfo.Ctx(ctx).Data(&in).InsertAndGetId()
	if err != nil {
		return nil, err
	}
	return &model.CategoryGroupCreateOutput{CategoryGroupId: in.CategoryGroupId}, nil
}

// CategoryGroupDelete 删除分类组
func (s *sCategoryGroup) CategoryGroupDelete(ctx context.Context, in *model.CategoryGroupDeleteInput) error {
	// 检查当前分类组是否存在
	err := checkCategoryGroupExist(ctx, in.CategoryGroupId)
	if err != nil {
		return err
	}
	// 执行删除
	return dao.CategoryGroupInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.CategoryGroupInfo.Ctx(ctx).Where(dao.CategoryGroupInfo.Columns().CategoryGroupId, in.CategoryGroupId).Delete()
		if err != nil {
			return err
		}
		return err
	})
}

// CategoryGroupUpdate 更新分组类
func (s *sCategoryGroup) CategoryGroupUpdate(ctx context.Context, in *model.CategoryGroupUpdateInput) error {
	// 检查分类组是否存在
	err := checkCategoryGroupExist(ctx, in.CategoryGroupId)
	if err != nil {
		return err
	}
	return dao.CategoryGroupInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		// 更新分类组
		_, err := dao.CategoryGroupInfo.Ctx(ctx).
			OmitEmpty().
			Data(&in).
			FieldsEx(dao.CategoryGroupInfo.Columns().CategoryGroupId).
			Where(dao.CategoryGroupInfo.Columns().CategoryGroupId, in.CategoryGroupId).
			Update()
		if err != nil {
			return err
		}
		return nil
	})
}

// CategoryGroupGetList 获取分类组列表
func (s *sCategoryGroup) CategoryGroupGetList(ctx context.Context, in *model.CategoryGroupGetListInput) (out *model.CategoryGroupGetListOutput, err error) {
	m := dao.CategoryGroupInfo.Ctx(ctx)
	out = &model.CategoryGroupGetListOutput{
		CommonPaginationOutput: model.CommonPaginationOutput{
			PageNum:  in.PageNum,
			PageSize: in.PageSize,
		},
	}
	// 分页查询，按创建时间倒序排序
	listModel := m.Page(in.PageNum, in.PageSize).OrderDesc(dao.CategoryGroupInfo.Columns().CreatedAt)
	// 执行查询
	var list []*entity.CategoryGroupInfo
	err = listModel.Scan(&list)
	if err != nil {
		return nil, err
	}
	// 如果没有数据
	if len(list) == 0 {
		return out, err
	}
	// 统计数量
	out.Total, err = m.Count()
	if err != nil {
		return out, err
	}
	err = listModel.Scan(&out.Lists)
	if err != nil {
		return out, err
	}
	return
}

// =============== private method =====================
func checkCategoryGroupExist(ctx context.Context, id string) error {
	count, err := dao.CategoryGroupInfo.Ctx(ctx).Where(dao.CategoryGroupInfo.Columns().CategoryGroupId, id).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeInvalidParameter, "不存在的分类组")
	}
	return nil
}
