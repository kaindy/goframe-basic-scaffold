package permission

import (
	"context"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"
)

type sPermission struct{}

func init() {
	service.RegisterPermission(New())
}

func New() *sPermission {
	return &sPermission{}
}

func (s *sPermission) Create(ctx context.Context, in *model.PermissionCreateInput) (out *model.PermissionCreateOutput, err error) {
	lastInsertId, err := dao.PermissionInfo.Ctx(ctx).Data(&in).InsertAndGetId()
	if err != nil {
		return nil, err
	}
	return &model.PermissionCreateOutput{Id: uint(lastInsertId)}, nil
}

func (s *sPermission) Delete(ctx context.Context, id uint) error {
	// 查询当前资源是否存在
	err := _checkPermissionExists(ctx, id)
	if err != nil {
		return err
	}
	// 执行删除
	return dao.PermissionInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.PermissionInfo.Ctx(ctx).WherePri(id).Delete()
		if err != nil {
			return err
		}
		return err
	})
}

func (s *sPermission) Update(ctx context.Context, in *model.PermissionUpdateInput) error {
	// 检查当前要更新的权限ID是否存在
	err := _checkPermissionExists(ctx, in.Id)
	if err != nil {
		return err
	}
	// 执行更新
	return dao.PermissionInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.PermissionInfo.Ctx(ctx).
			OmitEmpty().
			Data(&in).
			FieldsEx(dao.PermissionInfo.Columns().Id).
			Where(dao.PermissionInfo.Columns().Id, in.Id).
			Update()
		if err != nil {
			return err
		}
		return err
	})
}

func (s *sPermission) GetList(ctx context.Context, in *model.PermissionGetListInput) (out *model.PermissionGetListOutput, err error) {
	var m = dao.PermissionInfo.Ctx(ctx)
	out = &model.PermissionGetListOutput{
		CommonPaginationOutput: model.CommonPaginationOutput{
			PageNum:  in.PageNum,
			PageSize: in.PageSize,
		},
	}
	// 分页查询，按创建时间倒序排列
	listModel := m.Page(in.PageNum, in.PageSize).OrderDesc(dao.PermissionInfo.Columns().CreatedAt)
	// 执行查询
	var list []*entity.PermissionInfo
	err = listModel.Scan(&list)
	if err != nil {
		return nil, err
	}
	// 如果没有数据
	if len(list) == 0 {
		return out, nil
	}
	// 统计数量
	out.Total, err = m.Count()
	if err != nil {
		return nil, err
	}
	if err := listModel.Scan(&out.Lists); err != nil {
		return out, err
	}
	return
}

// 通过ID查询当前资源是否存在
func _checkPermissionExists(ctx context.Context, id uint) error {
	count, err := dao.PermissionInfo.Ctx(ctx).WherePri(id).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "不存在的权限ID")
	}
	return nil
}
