// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package logic

import (
	_ "goframe-basic-scaffold/internal/logic/category_group"
	_ "goframe-basic-scaffold/internal/logic/file"
	_ "goframe-basic-scaffold/internal/logic/permission"
	_ "goframe-basic-scaffold/internal/logic/role"
	_ "goframe-basic-scaffold/internal/logic/user"
)
