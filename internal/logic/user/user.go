package user

import (
	"context"
	"database/sql"
	"errors"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility"
	"goframe-basic-scaffold/utility/bizcode"
	"goframe-basic-scaffold/utility/snowflake"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/grand"
)

type sUser struct{}

func init() {
	service.RegisterUser(New())
}

func New() *sUser {
	return &sUser{}
}

// GetUserByNamePassword 通过账号密码获取用户信息
func (s *sUser) GetUserByNamePassword(ctx context.Context, in model.UserLoginInput) (map[string]interface{}, error) {
	// 检查用户是否存在
	adminInfo := &entity.AdminInfo{}
	err := dao.AdminInfo.Ctx(ctx).Where("name", in.Username).Scan(&adminInfo)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, gerror.New("账号不存在")
	}
	// 检查用户密码是否正确
	if utility.EncryptPassword(in.Password, adminInfo.UserSalt) != adminInfo.Password {
		return nil, gerror.New("账号或密码错误")
	}

	return g.Map{
		"uid":      adminInfo.Id,
		"username": adminInfo.Name,
	}, nil
}

// CreateUser 创建新用户
func (s *sUser) CreateUser(ctx context.Context, in *model.UserCreateInput) (out *model.UserCreateOutput, err error) {
	// 查询用户名是否已存在
	i, err := dao.AdminInfo.Ctx(ctx).Where("name", in.Name).Count()
	if err != nil {
		return nil, err
	}
	if i > 0 {
		return nil, gerror.New("已存在同名用户")
	}
	// 生成随机的加密盐
	UserSalt := grand.S(10)
	// 更新加密密码
	in.Password = utility.EncryptPassword(in.Password, UserSalt)
	in.UserSalt = UserSalt
	// 生成唯一ID
	in.Uid = snowflake.GenerateSFID()
	// 插入更新
	_, err = dao.AdminInfo.Ctx(ctx).Data(in).InsertAndGetId()
	if err != nil {
		return nil, err
	}

	return &model.UserCreateOutput{Uid: in.Uid}, nil
}

// DeleteUser 删除用户
func (s *sUser) DeleteUser(ctx context.Context, uid string) error {
	// 检查当前ID的用户是否存在
	count, err := dao.AdminInfo.Ctx(ctx).Where(dao.AdminInfo.Columns().Uid, uid).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "不存在的用户ID")
	}

	// 开启事务执行删除
	return dao.AdminInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.AdminInfo.Ctx(ctx).Where(dao.AdminInfo.Columns().Uid, uid).Delete()
		if err != nil {
			return err
		}
		return err
	})
}

// UpdateUser 修改用户
func (s *sUser) UpdateUser(ctx context.Context, in *model.UserUpdateInput) error {
	// 检查当前要修改的用户是否存在
	err := _checkUserExistsById(ctx, in.Uid)
	if err != nil {
		return err
	}
	// 更新
	return dao.AdminInfo.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = dao.AdminInfo.Ctx(ctx).
			OmitEmpty().
			Data(&in).
			FieldsEx(dao.AdminInfo.Columns().Id).
			Where(dao.AdminInfo.Columns().Uid, in.Uid).
			Update()
		if err != nil {
			return err
		}
		return err
	})
}

// GetUserList 获取用户列表
func (s *sUser) GetUserList(ctx context.Context, in *model.UserGetListInput) (out *model.UserGetListOutput, err error) {
	var m = dao.AdminInfo.Ctx(ctx)
	out = &model.UserGetListOutput{
		PaginationRes: common.PaginationRes{
			PageNum:  in.PageNum,
			PageSize: in.PageSize,
		},
	}
	// 分页查询，按创建时间倒序排序
	listModel := m.Page(in.PageNum, in.PageSize).OrderDesc(dao.AdminInfo.Columns().CreatedAt)
	// 执行查询
	var list []*entity.AdminInfo
	if err := listModel.Scan(&list); err != nil {
		return out, err
	}
	// 没有数据
	if len(list) == 0 {
		return out, nil
	}
	// 统计数量
	out.Total, err = m.Count()
	if err != nil {
		return out, err
	}
	if err := listModel.Scan(&out.Lists); err != nil {
		return out, err
	}
	return
}

// UpdateUserPassword 更新用户密码
func (s *sUser) UpdateUserPassword(ctx context.Context, in *model.UserUpdatePasswordInput) error {
	// 检查该用户是否存在
	err := _checkUserExistsById(ctx, in.Uid)
	if err != nil {
		return err
	}
	// 检查就密码是否正确
	err = _checkPasswordCorrect(ctx, in.Uid, in.Password)
	if err != nil {
		return err
	}
	// 创建新的加密盐
	in.UserSalt = grand.S(10)
	in.Password = utility.EncryptPassword(in.Password, in.UserSalt)
	// 更新
	_, err = dao.AdminInfo.Ctx(ctx).
		OmitEmpty().
		Data(&in).
		FieldsEx(dao.AdminInfo.Columns().Uid).
		Where(dao.AdminInfo.Columns().Uid, in.Uid).
		Update()
	if err != nil {
		return err
	}
	return err
}

// ==================== private method =============================

// _checkUserExistsById 检查用户是否存在
func _checkUserExistsById(ctx context.Context, uid string) error {
	count, err := dao.AdminInfo.Ctx(ctx).Where(dao.AdminInfo.Columns().Uid, uid).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "不存在的用户")
	}
	return nil
}

// _checkPasswordCorrect 检查密码是否正确
func _checkPasswordCorrect(ctx context.Context, uid string, password string) error {
	// 通过id查询当当前用户的加密盐和加密后的密码
	var _userInfo *entity.AdminInfo
	err := dao.AdminInfo.Ctx(ctx).Where(dao.AdminInfo.Columns().Uid, uid).Scan(&_userInfo)
	if err != nil {
		return err
	}
	// 通过获取的加密盐加密密码并与当前传递参数密码比较是否正确
	if utility.EncryptPassword(password, _userInfo.UserSalt) != _userInfo.Password {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "旧密码不正确")
	}
	return nil
}
