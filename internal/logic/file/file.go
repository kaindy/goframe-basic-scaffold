package file

import (
	"context"
	"goframe-basic-scaffold/internal/consts"
	"goframe-basic-scaffold/internal/dao"
	"goframe-basic-scaffold/internal/model"
	"goframe-basic-scaffold/internal/model/entity"
	"goframe-basic-scaffold/internal/service"
	"goframe-basic-scaffold/utility/bizcode"
	"os"
	"time"

	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/qiniu/go-sdk/v7/auth/qbox"
	"github.com/qiniu/go-sdk/v7/storage"
)

type sFile struct{}

func init() {
	service.RegisterFile(New())
}

func New() *sFile {
	return &sFile{}
}

// Upload 上传单个文件到本地
func (s *sFile) Upload(ctx context.Context, in *model.FileUploadInput) (out *model.FileUploadOutput, err error) {
	// 读取系统配置的临时文件存储位置
	uploadPath, err := getUploadPath(ctx)
	if err != nil {
		return nil, err
	}
	// 如果传入了文件名，则使用传入的文件名
	if in.Name != "" {
		in.File.Filename = in.Name
	}
	// 安全校验，每位用户每1分钟只能上传10次
	if err = verifySecret(ctx); err != nil {
		return nil, err
	}
	// 以年月日定义上传目录名称
	dateDirName := gtime.Now().Format("Ymd")
	// 保存文件
	fileName, err := in.File.Save(gfile.Join(uploadPath, dateDirName), in.RandomName)
	if err != nil {
		return nil, err
	}
	// 将文件信息写入数据库
	data := entity.FileInfo{
		Name:   fileName,
		Src:    gfile.Join(uploadPath, dateDirName, fileName),
		Url:    "/upload/" + dateDirName + "/" + fileName,
		UserId: gconv.Int(service.Auth().GetIdentity(ctx)),
	}
	lastInsertId, err := dao.FileInfo.Ctx(ctx).Data(&data).OmitEmpty().InsertAndGetId()
	if err != nil {
		return nil, err
	}
	return &model.FileUploadOutput{
		Id:   uint(lastInsertId),
		Name: data.Name,
		Src:  data.Src,
		Url:  data.Url,
	}, nil
}

// MultipartUpload 上传多个文件到本地
func (s *sFile) MultipartUpload(ctx context.Context, in *model.FilesUploadInput) (out []*model.FileUploadOutput, err error) {
	// 读取系统配置的临时文件存储位置
	uploadPath, err := getUploadPath(ctx)
	if err != nil {
		return nil, err
	}
	// 安全校验，每位用户每1分钟只能上传10次
	if err = verifySecret(ctx); err != nil {
		return nil, err
	}
	// 以年月日定义上传目录名称
	dateDirName := gtime.Now().Format("Ymd")
	// 定义返回的切片对象
	var uploadFileSlice []*model.FileUploadOutput
	// 循环保存文件
	for _, file := range *in.Files {
		fileName, err := file.Save(gfile.Join(uploadPath, dateDirName), in.RandomName)
		if err != nil {
			return nil, err
		}
		// 将文件信息写入数据库
		data := entity.FileInfo{
			Name:   fileName,
			Src:    gfile.Join(uploadPath, dateDirName, fileName),
			Url:    "/upload/" + dateDirName + "/" + fileName,
			UserId: gconv.Int(service.Auth().GetIdentity(ctx)),
		}
		lastInsertId, err := dao.FileInfo.Ctx(ctx).Data(&data).OmitEmpty().InsertAndGetId()
		if err != nil {
			return nil, err
		}
		uploadFileItem := &model.FileUploadOutput{
			Id:   uint(lastInsertId),
			Name: fileName,
			Src:  data.Src,
			Url:  data.Url,
		}
		uploadFileSlice = append(uploadFileSlice, uploadFileItem)
	}
	return uploadFileSlice, nil
}

// UploadFileToCloud 上传文件到SSO存储空间，支持单个或多个文件
func (s *sFile) UploadFileToCloud(ctx context.Context, in *model.FilesUploadCloudInput) (out []*model.FileUploadOutput, err error) {
	// 定义返回的切片对象
	var uploadFileSlice []*model.FileUploadOutput
	// 循环传入的上传文件切片对象，逐个进行上传
	for _, file := range *in.Files {
		name, url, err := uploadFileToCloud(ctx, file, in.RandomName)
		if err != nil {
			return nil, err
		}
		// 将文件信息写入数据库
		data := entity.FileInfo{
			Name:   name,
			Src:    "",
			Url:    url,
			UserId: gconv.Int(service.Auth().GetIdentity(ctx)),
		}
		lastInsertId, err := dao.FileInfo.Ctx(ctx).Data(&data).OmitEmpty().InsertAndGetId()
		if err != nil {
			return nil, err
		}
		uploadFileItem := &model.FileUploadOutput{
			Id:   uint(lastInsertId),
			Name: name,
			Src:  data.Src,
			Url:  data.Url,
		}
		uploadFileSlice = append(uploadFileSlice, uploadFileItem)
	}
	return uploadFileSlice, nil
}

// ============ private function ======================

// 获取上传文件路径的配置
func getUploadPath(ctx context.Context) (string, error) {
	uploadPath := g.Cfg().MustGet(ctx, "upload.path").String()
	if uploadPath == "" {
		return "", gerror.New("读取配置文件失败，上传路径不存在")
	}
	return uploadPath, nil
}

// 验证用户上传的频率，1分钟只能上传10次
func verifySecret(ctx context.Context) error {
	count, err := dao.FileInfo.Ctx(ctx).
		Where(dao.FileInfo.Columns().UserId, gconv.Uint(service.Auth().GetIdentity(ctx))).
		WhereGTE(dao.FileInfo.Columns().CreatedAt, gtime.Now().Add(time.Minute)).Count()
	if err != nil {
		return err
	}
	if count >= consts.FileMaxUploadCountMinute {
		return gerror.NewCode(bizcode.CodeBusinessValidationFailed, "上传太频繁，1分钟只能上传10次")
	}
	return nil
}

// uploadFileToCloud 上传单个文件到SSO空间，并写库，返回当前文件的访问URL地址
func uploadFileToCloud(ctx context.Context, file *ghttp.UploadFile, randomName bool) (name, url string, err error) {
	// 定义本地文件上传路径
	dirPath := g.Cfg().MustGet(ctx, "upload.path").String() + "/"
	// 保存上传的文件
	name, err = file.Save(dirPath, randomName)
	if err != nil {
		return "", "", err
	}
	// 定义本地文件路径
	localFilePath := dirPath + name
	// 七牛云配置参数
	bucket := g.Cfg().MustGet(ctx, "upload.qiniu.bucket").String()
	accessKey := g.Cfg().MustGet(ctx, "upload.qiniu.accessKey").String()
	secretKey := g.Cfg().MustGet(ctx, "upload.qiniu.secretKey").String()
	// 对接七牛云SDK
	putPolicy := storage.PutPolicy{
		Scope: bucket,
	}
	// 生成七牛token
	mac := qbox.NewMac(accessKey, secretKey)
	// 上传token
	upToken := putPolicy.UploadToken(mac)
	// 七牛上传配置
	cfg := storage.Config{}
	cfg.Zone = &storage.ZoneHuanan
	cfg.UseHTTPS = true
	cfg.UseCdnDomains = false
	// 构建表单上传对象
	formUploader := storage.NewFormUploader(&cfg)
	// 定义上传结果结构体
	ret := storage.PutRet{}
	// 可选配置
	putExtra := storage.PutExtra{
		Params: map[string]string{},
	}
	// 七牛表单上传
	err = formUploader.PutFile(ctx, &ret, upToken, name, localFilePath, &putExtra)
	if err != nil {
		return "", "", err
	}
	// 删除本地文件
	err = os.RemoveAll(localFilePath)
	if err != nil {
		return "", "", err
	}
	// 返回数据
	url = ret.Key
	return
}
