// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. 
// =================================================================================

package user

import (
	"context"
	
	"goframe-basic-scaffold/api/user/v1"
)

type IUserV1 interface {
	UserGetInfo(ctx context.Context, req *v1.UserGetInfoReq) (res *v1.UserGetInfoRes, err error)
	UserCreate(ctx context.Context, req *v1.UserCreateReq) (res *v1.UserCreateRes, err error)
	UserDelete(ctx context.Context, req *v1.UserDeleteReq) (res *v1.UserDeleteRes, err error)
	UserUpdate(ctx context.Context, req *v1.UserUpdateReq) (res *v1.UserUpdateRes, err error)
	UserGetList(ctx context.Context, req *v1.UserGetListReq) (res *v1.UserGetListRes, err error)
	UserUpdatePassword(ctx context.Context, req *v1.UserUpdatePasswordReq) (res *v1.UserUpdatePasswordRes, err error)
}


