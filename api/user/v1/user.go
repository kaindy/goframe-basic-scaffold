package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"goframe-basic-scaffold/api/common"
)

type UserGetInfoReq struct {
	g.Meta `path:"/user/getInfo" method:"get" tags:"User" summary:"获取用户信息"`
}

type UserGetInfoRes struct {
	Id          uint        `json:"id"`
	IdentityKey string      `json:"identity_key"`
	Payload     interface{} `json:"payload"`
}

type UserCreateReq struct {
	g.Meta     `path:"/user/create" method:"post" tags:"User" summary:"创建新用户"`
	Name       string `json:"name" v:"name@required|length:6,30#用户名不能为空|账号长度不合法(6-30位)" dc:"用户名"`
	Password   string `json:"password" v:"password@required|length:6,30#密码不能为空|密码长度不合法(6-30位)" dc:"用户密码"`
	RePassword string `json:"re_password" v:"repassword@required|length:6,30|same:password#确认密码不能为空|确认密码长度不合法(6-30位)|两次输入密码不一致" dc:"确认密码"`
	RoleIds    string `json:"role_ids" dc:"角色ids"`
	IsAdmin    int8   `json:"is_admin" dc:"是否为超级管理员"`
}

type UserCreateRes struct {
	Uid string `json:"uid"`
}

type UserDeleteReq struct {
	g.Meta `path:"/user/delete" method:"delete" tags:"User" summary:"删除用户"`
	Uid    string `json:"uid" v:"required#缺少用户ID" dc:"用户ID"`
}

type UserDeleteRes struct{}

type UserUpdateReq struct {
	g.Meta  `path:"/user/update" method:"post" tags:"User" summary:"更新用户"`
	Uid     string `json:"id" v:"required#请确定要修改的用户" dc:"用户ID"`
	RoleIds string `json:"role_ids" dc:"角色ids"`
	IsAdmin int8   `json:"is_admin" dc:"是否为管理员"`
}

type UserUpdateRes struct {
	Uid string `json:"uid"`
}

type UserGetListReq struct {
	g.Meta `path:"/user/list" method:"get" tags:"User" summary:"获取用户列表"`
	common.PaginationReq
}

type UserGetListRes struct {
	Lists interface{} `json:"lists"`
	common.PaginationRes
}

type UserUpdatePasswordReq struct {
	g.Meta      `path:"/user/update_passwd" method:"post" tags:"User" summary:"更新用户密码"`
	Uid         string `json:"uid" v:"required#请选择要更新密码的用户" dc:"用户ID"`
	OldPassword string `json:"old_password" v:"required#缺少旧密码" dc:"当前密码"`
	Password    string `json:"password" v:"password@required|length:6,30#密码不能为空|密码长度不合法(6-30位)" dc:"用户密码"`
	RePassword  string `json:"re_password" v:"repassword@required|length:6,30|same:password#确认密码不能为空|确认密码长度不合法(6-30位)|两次输入密码不一致" dc:"确认密码"`
}

type UserUpdatePasswordRes struct{}
