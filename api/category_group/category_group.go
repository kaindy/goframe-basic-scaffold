// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. 
// =================================================================================

package category_group

import (
	"context"
	
	"goframe-basic-scaffold/api/category_group/v1"
)

type ICategoryGroupV1 interface {
	CategoryGroupCreate(ctx context.Context, req *v1.CategoryGroupCreateReq) (res *v1.CategoryGroupCreateRes, err error)
	CategoryGroupDelete(ctx context.Context, req *v1.CategoryGroupDeleteReq) (res *v1.CategoryGroupDeleteRes, err error)
	CategoryGroupUpdate(ctx context.Context, req *v1.CategoryGroupUpdateReq) (res *v1.CategoryGroupUpdateRes, err error)
	CategoryGroupGetList(ctx context.Context, req *v1.CategoryGroupGetListReq) (res *v1.CategoryGroupGetListRes, err error)
}


