package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
)

type CategoryGroupCreateReq struct {
	g.Meta                   `path:"/category_group/create" method:"post" tags:"CategoryGroup" sm:"创建分类组"`
	CategoryGroupName        string `json:"category_group_name" v:"required#缺少分类组名称" dc:"分类组名称"`
	CategoryGroupDescription string `json:"category_group_description" dc:"分类组描述"`
}

type CategoryGroupCreateRes struct {
	CategoryGroupId string `json:"category_group_id" dc:"分类组ID"`
}

type CategoryGroupDeleteReq struct {
	g.Meta          `path:"/category_group/delete" method:"delete" tags:"CategoryGroup" sm:"删除分类组"`
	CategoryGroupId string `json:"category_group_id" v:"required#缺少分类组ID" dc:"分类组ID"`
}

type CategoryGroupDeleteRes struct{}

type CategoryGroupUpdateReq struct {
	g.Meta                   `path:"/category_group/update" method:"put" tags:"CategoryGroup" sm:"更新分类组"`
	CategoryGroupId          string `json:"category_group_id" v:"required#缺少分类组ID" dc:"分类组ID"`
	CategoryGroupName        string `json:"category_group_name" dc:"分类组名称"`
	CategoryGroupDescription string `json:"category_group_description" dc:"分类组描述"`
}

type CategoryGroupUpdateRes struct {
	CategoryGroupId string `json:"category_group_id" dc:"分类组ID"`
}

type CategoryGroupGetListReq struct {
	g.Meta `path:"/category_group/list" method:"get" tags:"CategoryGroup" sm:"获取分类组列表"`
	common.PaginationReq
}

type CategoryGroupGetListRes struct {
	Lists []*model.CategoryGroupItem `json:"lists" dc:"分类组列表"`
	common.PaginationRes
}
