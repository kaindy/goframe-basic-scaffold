// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. 
// =================================================================================

package file

import (
	"context"
	
	"goframe-basic-scaffold/api/file/v1"
)

type IFileV1 interface {
	FileUpload(ctx context.Context, req *v1.FileUploadReq) (res *v1.FileUploadRes, err error)
	FilesUpload(ctx context.Context, req *v1.FilesUploadReq) (res *v1.FilesUploadRes, err error)
	FilesUploadCloud(ctx context.Context, req *v1.FilesUploadCloudReq) (res *v1.FilesUploadCloudRes, err error)
}


