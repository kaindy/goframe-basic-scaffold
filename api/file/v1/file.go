package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"goframe-basic-scaffold/internal/model"
)

type FileUploadReq struct {
	g.Meta     `path:"/file/upload" method:"post" mime:"multipart/form-data" tags:"File" sm:"文件上传到本地"`
	File       *ghttp.UploadFile `json:"file" type:"file" dc:"上传文件"`
	Name       string            `json:"name" dc:"指定上传文件名"`
	RandomName bool              `json:"randomName" dc:"是否随机名"`
}

type FileUploadRes struct {
	Name string `json:"name" dc:"文件名称"`
	Url  string `json:"url" dc:"文件访问URL"`
}

type FilesUploadReq struct {
	g.Meta     `path:"/files/upload" method:"post" mime:"multipart/form-data" tags:"File" sm:"多文件上传到本地"`
	Files      *ghttp.UploadFiles `json:"files" dc:"上传文件"`
	RandomName bool               `json:"random_name" dc:"是否随机名称"`
}

type FilesUploadRes struct {
	Files []*model.FileUploadOutput `json:"files"`
}

type FilesUploadCloudReq struct {
	g.Meta     `path:"/files/upload_cloud" method:"post" mime:"multipart/form-data" tags:"File" sm:"多文件上传到云端"`
	Files      *ghttp.UploadFiles `json:"files" dc:"上传文件"`
	RandomName bool               `json:"random_name" dc:"是否随机名称"`
}

type FilesUploadCloudRes struct {
	Files []*model.FileUploadOutput `json:"files"`
}
