package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"goframe-basic-scaffold/api/common"
)

type RoleCreateReq struct {
	g.Meta `path:"/role/create" method:"post" tags:"Role" summary:"创建角色"`
	Name   string `json:"name" v:"required#角色名不能为空" dc:"角色名称"`
	Desc   string `json:"desc" dc:"角色描述"`
}

type RoleCreateRes struct {
	Id uint `json:"id"`
}

type RoleDeleteReq struct {
	g.Meta `path:"/role/delete" method:"delete" tags:"Role" summary:"删除角色"`
	Id     uint `json:"id" v:"min:1#缺少要删除的角色ID" dc:"角色ID"`
}

type RoleDeleteRes struct{}

type RoleUpdateReq struct {
	g.Meta `path:"/role/update" method:"post" tags:"Role" summary:"更新角色"`
	Id     uint   `json:"id" v:"min:1#缺少要更新的角色ID" dc:"角色ID"`
	Name   string `json:"name" dc:"角色名称"`
	Desc   string `json:"desc" dc:"角色描述"`
}

type RoleUpdateRes struct {
	Id uint `json:"id"`
}

type RoleGetListReq struct {
	g.Meta `path:"/role/list" method:"get" tags:"Role" summary:"角色列表"`
	common.PaginationReq
}

type RoleGetListRes struct {
	Lists interface{} `json:"lists"`
	common.PaginationRes
}

type RoleAddPermissionReq struct {
	g.Meta       `path:"/role/add/permission" method:"post" tags:"Role" summary:"角色添加权限"`
	RoleId       uint `json:"role_id" v:"required#缺少角色ID" dc:"角色ID"`
	PermissionId uint `json:"permission_id" v:"required#缺少权限ID" dc:"权限ID"`
}

type RoleAddPermissionRes struct {
	Id uint `json:"id"`
}

type RoleDeletePermissionReq struct {
	g.Meta       `path:"/role/delete/permission" method:"delete" tags:"Role" summary:"删除角色权限"`
	RoleId       uint `json:"role_id" v:"required#缺少角色ID" dc:"角色ID"`
	PermissionId uint `json:"permission_id" v:"required#缺少权限ID" dc:"权限ID"`
}

type RoleDeletePermissionRes struct{}
