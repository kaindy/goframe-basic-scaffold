// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. 
// =================================================================================

package role

import (
	"context"
	
	"goframe-basic-scaffold/api/role/v1"
)

type IRoleV1 interface {
	RoleCreate(ctx context.Context, req *v1.RoleCreateReq) (res *v1.RoleCreateRes, err error)
	RoleDelete(ctx context.Context, req *v1.RoleDeleteReq) (res *v1.RoleDeleteRes, err error)
	RoleUpdate(ctx context.Context, req *v1.RoleUpdateReq) (res *v1.RoleUpdateRes, err error)
	RoleGetList(ctx context.Context, req *v1.RoleGetListReq) (res *v1.RoleGetListRes, err error)
	RoleAddPermission(ctx context.Context, req *v1.RoleAddPermissionReq) (res *v1.RoleAddPermissionRes, err error)
	RoleDeletePermission(ctx context.Context, req *v1.RoleDeletePermissionReq) (res *v1.RoleDeletePermissionRes, err error)
}


