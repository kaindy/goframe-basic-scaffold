package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"time"
)

type AuthLoginReq struct {
	g.Meta   `path:"/auth/login" method:"post" tags:"Auth" summary:"执行登录请求"`
	Username string `json:"username" v:"required#请输入账号" dc:"用户账号"`
	Password string `json:"password" v:"required#请输入密码" dc:"用户密码"`
}

type AuthLoginRes struct {
	Token  string    `json:"token" dc:"token验证码"`
	Expire time.Time `json:"expire" dc:"过期时间"`
}

type AuthRefreshTokenReq struct {
	g.Meta `path:"/auth/refresh_token" method:"post" tags:"Auth" summary:"刷新Token"`
}

type AuthRefreshTokenRes struct {
	Token  string    `json:"token" dc:"token验证码"`
	Expire time.Time `json:"expire" dc:"过期时间"`
}

type AuthLogoutReq struct {
	g.Meta `path:"/auth/logout" method:"post" tags:"Auth" summary:"退出登录"`
}

type AuthLogoutRes struct{}
