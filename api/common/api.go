package common

import "github.com/gogf/gf/v2/os/gtime"

type PaginationReq struct {
	PageNum  int `json:"page_num" in:"query" d:"1" v:"min:0#分页号码错误" dc:"分页号, 默认1"`
	PageSize int `json:"page_size" in:"query" d:"10" v:"max:100#分页数量最大100条" dc:"分页数量, 最大100条"`
}

type PaginationRes struct {
	PageNum  int `json:"page_num" dc:"分页码"`
	PageSize int `json:"page_size" dc:"分页数量"`
	Total    int `json:"total" dc:"总数"`
}

type DatetimeOutput struct {
	CreatedAt *gtime.Time `json:"created_at" dc:"资源创建日期时间"`
	UpdatedAt *gtime.Time `json:"updated_at" dc:"资源最后更新日期时间"`
}
