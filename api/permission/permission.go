// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. 
// =================================================================================

package permission

import (
	"context"
	
	"goframe-basic-scaffold/api/permission/v1"
)

type IPermissionV1 interface {
	PermissionCreate(ctx context.Context, req *v1.PermissionCreateReq) (res *v1.PermissionCreateRes, err error)
	PermissionDelete(ctx context.Context, req *v1.PermissionDeleteReq) (res *v1.PermissionDeleteRes, err error)
	PermissionUpdate(ctx context.Context, req *v1.PermissionUpdateReq) (res *v1.PermissionUpdateRes, err error)
	PermissionGetList(ctx context.Context, req *v1.PermissionGetListReq) (res *v1.PermissionGetListRes, err error)
}


