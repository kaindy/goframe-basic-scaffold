package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"goframe-basic-scaffold/api/common"
	"goframe-basic-scaffold/internal/model"
)

type PermissionCreateReq struct {
	g.Meta `path:"/permission/create" method:"post" tags:"Permission" summary:"新增权限"`
	Name   string `json:"name" v:"required#缺少权限名称" dc:"权限名称"`
	Path   string `json:"path" dc:"权限对应的业务显示路径"`
}

type PermissionCreateRes struct {
	Id uint `json:"id"`
}

type PermissionDeleteReq struct {
	g.Meta `path:"/permission/delete" method:"delete" tags:"Permission" summary:"删除权限"`
	Id     uint `json:"id" v:"min:1#缺少要删除的权限ID" dc:"权限ID"`
}

type PermissionDeleteRes struct{}

type PermissionUpdateReq struct {
	g.Meta `path:"/permission/update" method:"post" tags:"Permission" summary:"更新权限"`
	Id     uint   `json:"id" v:"min:1#缺少要更新的权限ID" dc:"权限ID"`
	Name   string `json:"name" dc:"权限名称"`
	Path   string `json:"path" dc:"权限对应的业务显示路径"`
}

type PermissionUpdateRes struct {
	Id uint `json:"id"`
}

type PermissionGetListReq struct {
	g.Meta `path:"/permission/list" method:"get" tags:"Permission" summary:"获取权限列表"`
	common.PaginationReq
}

type PermissionGetListRes struct {
	Lists []*model.PermissionListItem `json:"lists"`
	common.PaginationRes
}
